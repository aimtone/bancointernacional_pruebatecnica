package com.test.bancointernacional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancointernacionalApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancointernacionalApplication.class, args);
	}

}
