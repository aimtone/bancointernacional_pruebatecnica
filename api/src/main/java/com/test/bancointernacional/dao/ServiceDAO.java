package com.test.bancointernacional.dao;

import com.test.bancointernacional.dto.Service;

public interface ServiceDAO {

	public Service[] getTasksFromServices();
}
