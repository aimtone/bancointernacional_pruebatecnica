package com.test.bancointernacional.dao.impl;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.test.bancointernacional.dao.ServiceDAO;
import com.test.bancointernacional.dto.Service;

@Repository
public class ServiceDAOImpl implements ServiceDAO {

	@Override
	public Service[] getTasksFromServices() {
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8080/generator/schedule/tasks";
		ResponseEntity<Service[]> responseEntity = restTemplate.getForEntity(url, Service[].class);
		Service[] taskFromService = responseEntity.getBody();
		return taskFromService;
	}

}
