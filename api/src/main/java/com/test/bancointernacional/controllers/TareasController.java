package com.test.bancointernacional.controllers;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.test.bancointernacional.dao.ServiceDAO;
import com.test.bancointernacional.dto.Response;
import com.test.bancointernacional.dto.Service;
import com.test.bancointernacional.dto.Task;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET})
@RequestMapping("/")
@Api(value = "/", description = "Controlador de las tareas, este controlador se encarga de agrupar las tareas de manera que cada una de ellas puedan ser completadas en un lapso de 8 horas por defecto")
public class TareasController {

	@Autowired
	private ServiceDAO serviceDAO;

	@GetMapping("/")
	@ApiOperation(value = "Agrupa todas las tareas para ser completadas en un lapso de jornada laboral establecido", notes = "Devuelve las tareas agrupadas segun la configuración de los parametros")
	public Response get(@RequestParam(required = false, defaultValue = "8") int horas_jornada,
			@RequestParam(required = false, defaultValue = "false") boolean permite_pausar_tarea) {

		// Se obtiene tareas desde el microservicio
		Service[] taskFromService = serviceDAO.getTasksFromServices();

		// Se crea array que contiene la lista de tareas ya agrupadas
		List<Task> taskList = new ArrayList<Task>();

		int sumaHoras = 0;
		int horasDisponibles = horas_jornada;
		int index = 0;

		// Se crea array que almacena temporalmente las tareas que luego seran asignadas
		// a un dia especifico
		List<Service> taskNameList = new ArrayList<Service>();

		// Se itera sobre el array obtenido del microservicio
		for (Service object : taskFromService) {
			index += 1;

			// Suma las horas de las tareas
			sumaHoras = sumaHoras + object.getDuration();

			if (permite_pausar_tarea) {
				// Calcula el tiempo disonible de la jornada
				horasDisponibles = horas_jornada - sumaHoras;

				// Si la tarea actual es mayor que el tiempo disponible para esa jornada, se
				// asigna el tiempo disponible de esa jornada
				if (horasDisponibles < 0) {
					object.setDuration(object.getDuration() - Math.abs(horasDisponibles));
					object.setCompleted(false);
				}

				// Las tareas son agregadas al array temporal que las agrupa para una jornada
				taskNameList.add(object);

				// Si la suma de las horas sobrepasa el tiempo de la jornada, se debe agregar el
				// array creado al array principal de todas las jornadas
				if (sumaHoras >= horas_jornada) {
					addTask(taskList, taskNameList);

					// Se resetea el elemento que contiene las tareas para una jornada
					taskNameList = new ArrayList<Service>();

					// Si la ultima tarea no puedo ser completada en el lapso de la jornada diaria,
					// se agrega a un objeto auxiliar para mantenerlo con el tiempo restante
					if (Math.abs(horasDisponibles) != 0 && Math.abs(horasDisponibles) < horas_jornada) {
						Service auxObject = new Service(object.getTask_id(), object.getTask_name(),
								object.getDuration(), object.isCompleted());
						auxObject.setDuration(Math.abs(horasDisponibles));
						auxObject.setCompleted(true);
						taskNameList.add(auxObject);

						// Si es el ultimo elemento y restan horas por agregar del mismo, entonces lo
						// agrega de una vez al array principal de jornadas, ya que no volvera a iterar
						// sobre el objeto del microservicio
						if (index == taskFromService.length) {
							addTask(taskList, taskNameList);
						} else {
							// Si no es el ultimo objetos simplemente se asigna a la suma de las horas el
							// tiempo establecido y se calcula las horas disponibles para la siguiente
							// jornada
							sumaHoras = auxObject.getDuration();
							horasDisponibles = horas_jornada - sumaHoras;
						}

					} else {
						// Si las tareas encajan perfectamente en el tiempo, resetea los valores
						sumaHoras = 0;
						horasDisponibles = horas_jornada;
					}

				}
			} else {
				// Se calcula si las horas de las tareas sumadas encajan en la jornada laboral,
				// si es asi se agregan a la lista temporal de tareas
				if (sumaHoras <= horas_jornada) {
					taskNameList.add(object);
				} else {
					// De no encajar, se evalua que la lista contenga elementos, si es asi, los
					// agrega a la lista principal
					if (taskNameList.size() != 0) {

						addTask(taskList, taskNameList);

						// Se resetean los valores
						taskNameList = new ArrayList<Service>();
						sumaHoras = 0;

					}

					// Si la duracion del objeto encaja en el horario laboral, agrego la tarea al
					// array temporal de tareas de la siguiente jornada
					if (object.getDuration() <= horas_jornada) {
						taskNameList.add(object);
						sumaHoras = object.getDuration();
					}

				}
			}

		}

		Response response = new Response();
		response.setHoras_jornada(horas_jornada);
		response.setCantidad_dias(taskList.size());
		response.setJornadas(taskList);
		return response;
	}

	public void addTask(List<Task> taskList, List<Service> taskNameList) {
		Task task = new Task();
		task.setId(taskList.size() + 1);
		task.setTareas(taskNameList);
		taskList.add(task);
	}
}
