package com.test.bancointernacional.dto;

import java.util.List;

public class Task {

	public int id;
	public List<Service> tareas;

	public int getId() {
		return id;
	}

	public void setId(int i) {
		this.id = i;
	}

	public List<Service> getTareas() {
		return tareas;
	}

	public void setTareas(List<Service> tareas) {
		this.tareas = tareas;
	}

}
