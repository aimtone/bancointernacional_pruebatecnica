package com.test.bancointernacional.dto;

public class Service {

	public String task_id;
	public String task_name;
	public int duration;
	public boolean completed = true;

	public Service() {
	}

	public Service(String task_id, String task_name, int duration, boolean completed) {
		this.task_id = task_id;
		this.task_name = task_name;
		this.duration = duration;
		this.completed = completed;
	}

	public String getTask_id() {
		return task_id;
	}

	public void setTask_id(String task_id) {
		this.task_id = task_id;
	}

	public String getTask_name() {
		return task_name;
	}

	public void setTask_name(String task_name) {
		this.task_name = task_name;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

}
