package com.test.bancointernacional.dto;

import java.util.List;

public class Response {

	public int horas_jornada;
	public int cantidad_dias;
	public List<Task> jornadas;

	public int getHoras_jornada() {
		return horas_jornada;
	}

	public void setHoras_jornada(int horas_jornada) {
		this.horas_jornada = horas_jornada;
	}

	public int getCantidad_dias() {
		return cantidad_dias;
	}

	public void setCantidad_dias(int cantidad_dias) {
		this.cantidad_dias = cantidad_dias;
	}

	public List<Task> getJornadas() {
		return jornadas;
	}

	public void setJornadas(List<Task> jornadas) {
		this.jornadas = jornadas;
	}

	

}
