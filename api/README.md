# Version Java

- Java 11.0.11

# Como correr servidor en desarrollo

1) Ubicarse en la raiz del proyecto Spring y abrir una terminal
2) Ejecutar el siguiente comando ``` ./mvnw spring-boot:run```
3) Ingresa a *http://localhost:8081/swagger-ui.html* para ver la documentacion de la api o *http://localhost:8081/?horas_jornada=8&permite_pausar_tarea=false* para ver la respuesta directamente en el navegador
