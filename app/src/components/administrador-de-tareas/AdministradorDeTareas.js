import React, { Component } from "react";
import {
  ListGroup,
  Table,
  Col,
  Row,
  Tab,
  Form,
  Button,
  Alert,
} from "react-bootstrap";
import trueImg from "../../assets/images/true.svg";
import falseImg from "../../assets/images/false.svg";
import AdministradorDeTareaServices from "../../services/AdministradorDeTareas.services";

class AdministradorDeTareas extends Component {
  constructor(props) {
    super(props);
    this.state = {
      response: {},
      horas_jornada: 8,
      permite_pausar_tarea: false,
    };
    this.setHorasJornada = this.setHorasJornada.bind(this);
    this.setPermitePausarTareas = this.setPermitePausarTareas.bind(this);
  }

  componentDidMount() {
    this.getTareas();
  }

  getTareas = () => {
    let horas_jornada = this.state.horas_jornada;
    let permite_pausar_tarea = this.state.permite_pausar_tarea;
    AdministradorDeTareaServices.getInstance()
      .getTareas(horas_jornada, permite_pausar_tarea)
      .then((data) => {
        this.setState({ response: data });
      });
  };

  setHorasJornada(event) {
    this.setState({ horas_jornada: event.target.value });
  }
  setPermitePausarTareas(event) {
    this.setState({ permite_pausar_tarea: event.target.value });
  }

  render() {
    return (
      <div className="AdministradorDeTareas m-5">
        <Row>
          <Col>
            <h3>Administrador de Tareas</h3>
            <p>
              A continuación se muestran todas las tareas que pueden ser
              completadas en un lapse de{" "}
              {this.state.response != null && this.state.response.horas_jornada}{" "}
              horas diarias y la cantidad de dias que demorará en completar todo
              el proceso.
            </p>
          </Col>
        </Row>

        <Form>
          <Form.Row className="align-items-center mb-3">
            <Col xs="auto">
              <Form.Control
                size="sm"
                className="mb-2"
                type="number"
                id="horas_jornada"
                min="1"
                title="Jornada Laboral Actual"
                placeholder=""
                value={this.state.horas_jornada}
                onChange={this.setHorasJornada}
              />
            </Col>

            <Col xs="auto">
              <Form.Check
                type="checkbox"
                id="permite_pausar_tarea"
                className="mb-2"
                label="Permitir pausar tareas"
                title="Permite ejecutar la tarea a continuación completando la cantidad de horas restantes en la jornada laboral y pausandola para ejecutar las horas restantes al dia siguiente"
                checked={this.state.permite_pausar_tarea}
                onChange={this.setPermitePausarTareas}
              />
            </Col>
            <Col xs="auto">
              <Button
                size="sm"
                type="button"
                className="mb-2"
                onClick={this.getTareas}
                variant="primary"
              >
                Actualizar
              </Button>
            </Col>
          </Form.Row>
        </Form>

        {this.state.response == null && (
          <Alert variant="danger">
            No se ha obtenido respuesta desde el servidor, por favor, asegurese
            de que haya conexión con el mismo y recargue la página
          </Alert>
        )}
        {this.state.response != null && (
          <Tab.Container defaultActiveKey="#1">
            <Row>
              <Col sm={4}>
                <ListGroup>
                  {this.state.response.jornadas !== undefined &&
                    this.state.response.jornadas.map((jornada) => (
                      <ListGroup.Item
                        variant="light"
                        key={"item_" + jornada.id}
                        action
                        href={"#" + jornada.id}
                      >
                        Día {jornada.id}
                      </ListGroup.Item>
                    ))}
                </ListGroup>
              </Col>
              <Col sm={8}>
                <Tab.Content>
                  {this.state.response.jornadas !== undefined &&
                    this.state.response.jornadas.map((jornada) => (
                      <Tab.Pane
                        key={"content_" + jornada.id}
                        eventKey={"#" + jornada.id}
                      >
                        <Table
                          striped
                          variant="light"
                          bordered
                          hover
                          size="sm"
                          responsive="sm"
                        >
                          <thead>
                            <tr>
                              <th>Tarea</th>
                              <th>Duración</th>
                              <th>Tarea finalizada</th>
                            </tr>
                          </thead>
                          <tbody>
                            {jornada.tareas.map((tarea) => (
                              <tr key={tarea.task_id}>
                                <td title={tarea.task_id}>{tarea.task_name}</td>
                                <td>{tarea.duration} horas</td>
                                <td>
                                  {tarea.completed ? (
                                    <div>
                                      <img
                                        width="30"
                                        height="30"
                                        alt=""
                                        src={trueImg}
                                      />
                                      <span>Tarea finalizada</span>
                                    </div>
                                  ) : (
                                    <div>
                                      <img
                                        width="30"
                                        height="30"
                                        alt=""
                                        src={falseImg}
                                      />
                                      <span>
                                        Continuará en el día {jornada.id + 1}
                                      </span>
                                    </div>
                                  )}
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </Table>
                      </Tab.Pane>
                    ))}
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>
        )}
      </div>
    );
  }
}

export default AdministradorDeTareas;
