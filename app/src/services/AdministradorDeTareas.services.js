import { Component } from "react";

class AdministradorDeTareaServices extends Component {
  static myInstance = null;

  constructor() {
    super();
    this.host = "http://localhost:8081";
  }

  static getInstance() {
    return new AdministradorDeTareaServices();
  }

  async getTareas(horas_jornada, permite_pausar_tarea) {
    try {
      let response = await fetch(
        `${process.env.REACT_APP_API_URL}?horas_jornada=${horas_jornada}&permite_pausar_tarea=${permite_pausar_tarea}`
      );
      let responseJson = await response.json();
      return responseJson;
    } catch (error) {
      return null;
    }
  }
}
export default AdministradorDeTareaServices;
