import "bootstrap/dist/css/bootstrap.min.css";
import { Container , Row, Col} from "react-bootstrap";
import AdministradorDeTareas from "./components/administrador-de-tareas/AdministradorDeTareas";

function App() {
  return (
    <Container>
      <Row>
        <Col>
          <AdministradorDeTareas />
        </Col>
      </Row>
    </Container>
  );
}

export default App;
